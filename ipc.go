package main
/*
  Listens to a UNIX socket and forwards messages to the main function using a channel
*/

import (
  "net"
  "fmt"
  "bytes"
  "log"
  "io"
  
	"github.com/kjk/betterguid"
  "github.com/AdminXVII/go-libsse"
)

func newSource(conn net.Conn, uid string, msgs chan sse.Message){
  defer conn.Close()
  
  msgs <- sse.Message{ Event: "new", Data: fmt.Sprintf("{\"uid\":\"%s\"}", uid) }
  
  for {
    buf := make([]byte, 1500) // MTU size
    nr, err := conn.Read(buf)
    
    if err == io.EOF {
      msgs <- sse.Message{ Event: "end", Data: fmt.Sprintf("{\"uid\":\"%s\"}", uid) }
      fmt.Println("client quited")
      break
    } else if err != nil {
      fmt.Println("ERROR: Error while reading: ", err)
      break
    }
    
    data := bytes.Split(buf[:nr], []byte(": "))
    
    if len(data) != 2 {
      log.Print("ERROR: data is not in a valid format")
      continue
    }
    
    msgs <- sse.Message{ Event: string(data[0]), Data: fmt.Sprintf("{\"uid\":\"%s\",\"data\":\"%s\"}", uid,
      bytes.Replace(data[1], []byte("\n"), []byte("\\n"), -1)) }
  }
}

func SourceListener(unix net.Listener) chan sse.Message {
  // Create tasks channel
  msgs := make(chan sse.Message, 100)
  go func(){
    for {
      conn, err := unix.Accept()
      if err != nil {
        log.Print("ERROR: Error while opening connection with source: ", err)
      } else {
        go newSource(conn, betterguid.New(), msgs)
      }
    }
  }()
  return msgs
}
