package main

import (
  "os"
  "log"
  "net/http"
  
  "github.com/AdminXVII/go-libsse"
)

func main() {
  unix, tcp := Connections()
  defer unix.Close()
  
  msgs := SourceListener(unix)
  
  // Register with endpoint.
  srv := &http.Server{ Handler: CreateServer(msgs) }
  
  // Launch server
  srv.Serve(tcp)
}

func CreateServer(source chan sse.Message) *sse.Server {
  // Create the server.
  s := sse.NewServer(&sse.Options{
    Headers: map[string]string {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, OPTIONS",
      "Access-Control-Allow-Headers": "Keep-Alive,Cache-Control,Content-Type",
    },
    Logger: log.New(os.Stdout,
			"go-libsse: ",
			log.Ldate|log.Ltime),
  })
  
  go func(){
    for msg := range source {
      s.SendMessage(msg)
    }
  }()
  
  return s
}
